<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_clients';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "created at" column.
     *
     * @const string
     */
    const CREATED_AT = null;

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "updated at" column.
     *
     * @const string
     */
    const UPDATED_AT = null;

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'isp', 'idp', 'name', 'alamat', 'status', 'traffic_table', 'note', 'host', 'frame_id', 'slot_id', 'port_id', 'ont_id', 'sn_auth', 'management_mode', 'ont_lineprofile_id', 'ont_srvprofile_id', 'description', 'disabled', 'created_on', 'board_id', 'ont_lineprofile_name', 'ont_srvprofile_name', 'hostname', 'ont_autofind', 'traffic_table_cir', 'vendor_id', 'ont_version', 'ont_software_version', 'ont_equipment_id', 'native_vlan_eth_1', 'native_vlan_eth_2', 'native_vlan_eth_3', 'native_vlan_eth_4', 'vlan_1', 'gemport_1', 'user_vlan_1', 'tag_transform_1', 'vlan_2', 'gemport_2', 'user_vlan_2', 'tag_transform_2', 'traffic_table_1', 'traffic_table_2', 'inner_vlan_1', 'inner_vlan_2', 'index_1', 'index_2', 'ont_count', 'template', 'template_vlan', 'add_vlan', 'add_vlan_service', 'add_vlan_line', 'vlan_frame_id', 'vlan_slot_id', 'vlan_port_id', 'add_traffic_table',
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
