<?php

namespace App\Http\Controllers\API\v1;

use App\Item;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('Name', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'Name' => 'nullable|max:250',
            'PackageStatus' => 'nullable|integer|between:-9,9',
            'ItemBrandCode' => 'nullable|max:250',
            'ItemSubCategoryCode' => 'nullable|max:250',
            'UnitOfMeasureCode' => 'nullable|max:250',
            'InventoryType' => 'nullable|in:INVENTORY,NON_INVENTORY',
            'SerialNoStatus' => 'nullable|integer|between:-9,9',
            'MinStock' => 'nullable|between:-999999999999999999,999999999999999999',
            'MaxStock' => 'nullable|between:-999999999999999999,999999999999999999',
            'PackingCode' => 'nullable|max:250',
            'PackingQuantity' => 'nullable|between:-999999999999999999.9999,999999999999999999.9999',
            'KgQuantity' => 'nullable|between:-999999999999999999.9999,999999999999999999.9999',
            'ActiveStatus' => 'nullable|integer|between:-9,9',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
            'Remark' => 'nullable',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['Code' => 'required|max:250|unique:mst_item,Code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new Item;
        }

        if ($request->Code) $data->Code = $request->Code;
        if ($request->Name) $data->Name = $request->Name;
        if ($request->PackageStatus) $data->PackageStatus = $request->PackageStatus;
        if ($request->ItemBrandCode) $data->ItemBrandCode = $request->ItemBrandCode;
        if ($request->ItemSubCategoryCode) $data->ItemSubCategoryCode = $request->ItemSubCategoryCode;
        if ($request->UnitOfMeasureCode) $data->UnitOfMeasureCode = $request->UnitOfMeasureCode;
        if ($request->InventoryType) $data->InventoryType = $request->InventoryType;
        if ($request->SerialNoStatus) $data->SerialNoStatus = $request->SerialNoStatus;
        if ($request->MinStock) $data->MinStock = $request->MinStock;
        if ($request->MaxStock) $data->MaxStock = $request->MaxStock;
        if ($request->PackingCode) $data->PackingCode = $request->PackingCode;
        if ($request->PackingQuantity) $data->PackingQuantity = $request->PackingQuantity;
        if ($request->KgQuantity) $data->KgQuantity = $request->KgQuantity;
        if ($request->ActiveStatus) $data->ActiveStatus = $request->ActiveStatus;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        if ($request->InActiveBy) $data->InActiveBy = $request->InActiveBy;
        if ($request->InActiveDate) $data->InActiveDate = $request->InActiveDate;
        if ($request->Remark) $data->Remark = $request->Remark;
        $data->save();

        return $data;
    }
}
