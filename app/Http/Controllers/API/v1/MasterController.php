<?php

namespace App\Http\Controllers\API\v1;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display a listing of the resource.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */
    public function index($table, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'limit' => 'required_with:page|integer|min:1',
            'page' => 'required_with:limit|integer|min:1',
            'search' => 'nullable',
            'filter_border1_code' => 'nullable|max:250',
            'filter_border2_code' => 'nullable|max:250',
            'filter_border3_code' => 'nullable|max:250',
            'filter_distribution_code' => 'nullable|max:250',
            'filter_consentrator_code' => 'nullable|max:250',
            'filter_bts_code' => 'nullable|max:250',
            'filter_shaper1_code' => 'nullable|max:250',
            'filter_shaper2_code' => 'nullable|max:250',
            'filter_shaper3_code' => 'nullable  |max:250',
        ]);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $data = getModelName($table)::when($request->search, function ($query, $key) use ($table) {
                    return getControllerName($table)::searchQuery($query, $key);
                })
                ->when($request->filter_border1_code, function ($query, $key) {
                    return $query->where('Border1Code', $key);
                })
                ->when($request->filter_border2_code, function ($query, $key) {
                    return $query->where('Border2Code', $key);
                })
                ->when($request->filter_border3_code, function ($query, $key) {
                    return $query->where('Border3Code', $key);
                })
                ->when($request->filter_distribution_code, function ($query, $key) {
                    return $query->where('DistributionCode', $key);
                })
                ->when($request->filter_consentrator_code, function ($query, $key) {
                    return $query->where('ConsentratorCode', $key);
                })
                ->when($request->filter_bts_code, function ($query, $key) {
                    return $query->where('BTSCode', $key);
                })
                ->when($request->filter_shaper1_code, function ($query, $key) {
                    return $query->where('Shaper1Code', $key);
                })
                ->when($request->filter_shaper2_code, function ($query, $key) {
                    return $query->where('Shaper2Code', $key);
                })
                ->when($request->filter_shaper3_code, function ($query, $key) {
                    return $query->where('Shaper3Code', $key);
                })
                ->paginate($request->limit);

        return makeResponse(200, 'pagination', null, getResourceName($table)::collection($data));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Store a newly created resource in database.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @return json
     */


    public function store($table, Request $request)
    {
        $validator = getControllerName($table)::validation($request);

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $data = getControllerName($table)::save($request);

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.create] ' . json_encode(array_merge(['ClientIP' => $request->ip()], ['Data' => new $resource($data)])));

        return makeResponse(201, 'success', 'new ' . str_replace('-', ' ', $table) . ' has been save successfully', new $resource($data));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Display the specified resource.
     *
     * @param  string  $table
     * @param  string  $id
     * @return json
     */
    public function show($table, $id)
    {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));

        if (!$data) return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $resource = getResourceName($table);

        return makeResponse(200, 'success', null, new $resource($data));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Update the specified resource in database.
     *
     * @param  string  $table
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return json
     */
    public function update($table, Request $request, $id)
    {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));

        if (!$data) return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $validator = getControllerName($table)::validation($request, 'update');

        if ($validator->fails()) return makeResponse(400, 'error', 'validation is invalid', $validator->errors()->all());

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.edit] ' . json_encode(array_merge(['ClientIP' => $request->ip()], ['OldData' => new $resource($data), 'NewData' => array_merge([$data->getKeyName() => str_replace('%20', ' ', $id)], $request->all())])));

        $data = getControllerName($table)::save($request, $data);

        return makeResponse(200, 'success', str_replace('-', ' ', $table) . ' has been update successfully', new $resource($data));
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Remove the specified resource from database.
     *
     * @param  string  $table
     * @param  string  $id
     * @return json
     */
    public function destroy($table, $id)
    {
        $data = getModelName($table)::find(str_replace('%20', ' ', $id));

        if (!$data) return makeResponse(404, 'error', str_replace('-', ' ', $table) . ' not found');

        $resource = getResourceName($table);

        Log::info('[' . $data->getTable() . '.delete] ' . json_encode(array_merge(['ClientIP' => request()->ip()], ['Data' => new $resource($data)])));

        $data->delete();

        return makeResponse(200, 'success', str_replace('-', ' ', $table) . ' has been delete successfully');
    }
}
