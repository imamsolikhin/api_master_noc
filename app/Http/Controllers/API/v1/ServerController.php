<?php

namespace App\Http\Controllers\API\v1;

use App\Server;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ServerController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('name', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'code' => 'nullable|max:255',
            'name' => 'nullable|max:255',
            'rpc_endpoint' => 'nullable|max:255',
            'note' => 'nullable',
            'created_on' => 'nullable|date_format:Y-m-d H:i:s',
            'wamp_registration_id' => 'nullable|max:255',
            'ip_address' => 'nullable|max:255',
            'location' => 'nullable|max:255',
            'disabled' => 'nullable|max:1',
            'dev_uplink' => 'nullable|max:255',
            'dev_downlink' => 'nullable|max:255',
            'class_group' => 'nullable|max:255',
            'class_attr' => 'nullable|max:255',
            'service' => 'nullable|max:255',
            'ssh_user' => 'nullable|max:255',
            'ssh_pass' => 'nullable|max:255',
            'ActiveStatus' => 'nullable|integer|between:-9,9',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new Server;
        }

        if ($request->code) $data->code = $request->code;
        if ($request->name) $data->name = $request->name;
        if ($request->rpc_endpoint) $data->rpc_endpoint = $request->rpc_endpoint;
        if ($request->note) $data->note = $request->note;
        if ($request->created_on) $data->created_on = $request->created_on;
        if ($request->wamp_registration_id) $data->wamp_registration_id = $request->wamp_registration_id;
        if ($request->ip_address) $data->ip_address = $request->ip_address;
        if ($request->location) $data->location = $request->location;
        if ($request->disabled) $data->disabled = $request->disabled;
        if ($request->dev_uplink) $data->dev_uplink = $request->dev_uplink;
        if ($request->dev_downlink) $data->dev_downlink = $request->dev_downlink;
        if ($request->class_group) $data->class_group = $request->class_group;
        if ($request->class_attr) $data->class_attr = $request->class_attr;
        if ($request->service) $data->service = $request->service;
        if ($request->ssh_user) $data->ssh_user = $request->ssh_user;
        if ($request->ssh_pass) $data->ssh_pass = $request->ssh_pass;
        if ($request->ActiveStatus) $data->ActiveStatus = $request->ActiveStatus;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
