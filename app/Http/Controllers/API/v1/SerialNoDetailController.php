<?php

namespace App\Http\Controllers\API\v1;

use App\SerialNoDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SerialNoDetailController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('BranchCode', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'BranchCode' => 'required|max:50',
            'WarehouseCode' => 'required|max:50',
            'SerialNo' => 'required|max:100',
            'ItemCode' => 'required|max:50',
            'ActualStock' => 'required|between:-999999999999999999.9999,999999999999999999.9999',
            'COGSIDR' => 'required|between:-999999999999999999.9999,999999999999999999.9999',
            'LocationCode' => 'required|max:50',
            'RemarkText' => 'required',
            'RemarkDate' => 'required|date_format:Y-m-d H:i:s',
            'RemarkQuantity' => 'required|between:-999999999999999999.9999,999999999999999999.9999',
            'InDocumentType' => 'required|max:50',
            'InTransactionNo' => 'required|max:50',
            'InTransactionDate' => 'required|date_format:Y-m-d H:i:s',
            'OutDocumentType' => 'required|max:50',
            'OutTransactionNo' => 'required|max:50',
            'OutTransactionDate' => 'required|date_format:Y-m-d H:i:s',
            'OutStatus' => 'required|integer|between:-9,9',
            'CreatedBy' => 'required|max:250',
            'CreatedDate' => 'required|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['Code' => 'required|max:50|unique:mst_serial_no_detail,Code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new SerialNoDetail;
        }

        if ($request->Code) $data->Code = $request->Code;
        if ($request->BranchCode) $data->BranchCode = $request->BranchCode;
        if ($request->WarehouseCode) $data->WarehouseCode = $request->WarehouseCode;
        if ($request->SerialNo) $data->SerialNo = $request->SerialNo;
        if ($request->ItemCode) $data->ItemCode = $request->ItemCode;
        if ($request->ActualStock) $data->ActualStock = $request->ActualStock;
        if ($request->COGSIDR) $data->COGSIDR = $request->COGSIDR;
        if ($request->LocationCode) $data->LocationCode = $request->LocationCode;
        if ($request->RemarkText) $data->RemarkText = $request->RemarkText;
        if ($request->RemarkDate) $data->RemarkDate = $request->RemarkDate;
        if ($request->RemarkQuantity) $data->RemarkQuantity = $request->RemarkQuantity;
        if ($request->InDocumentType) $data->InDocumentType = $request->InDocumentType;
        if ($request->InTransactionNo) $data->InTransactionNo = $request->InTransactionNo;
        if ($request->InTransactionDate) $data->InTransactionDate = $request->InTransactionDate;
        if ($request->OutDocumentType) $data->OutDocumentType = $request->OutDocumentType;
        if ($request->OutTransactionNo) $data->OutTransactionNo = $request->OutTransactionNo;
        if ($request->OutTransactionDate) $data->OutTransactionDate = $request->OutTransactionDate;
        if ($request->OutStatus) $data->OutStatus = $request->OutStatus;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
