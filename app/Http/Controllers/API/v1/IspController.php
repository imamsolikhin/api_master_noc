<?php

namespace App\Http\Controllers\API\v1;

use App\Isp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class IspController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('name', 'like', '%' . $key . '%')
                    ->orWhere('phone', 'like', '%' . $key . '%')
                    ->orWhere('address', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'name' => 'nullable|max:255',
            'phone' => 'nullable|max:255',
            'address' => 'nullable|max:255',
            'created_by' => 'nullable|max:255',
            'created_at' => 'nullable|date_format:Y-m-d H:i:s',
            'updated_by' => 'nullable|max:255',
            'updated_at' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['id' => 'required|integer|unique:mst_isp,id']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new Isp;
        }

        if ($request->id) $data->id = $request->id;
        if ($request->name) $data->name = $request->name;
        if ($request->phone) $data->phone = $request->phone;
        if ($request->address) $data->address = $request->address;
        if ($request->created_by) $data->created_by = $request->created_by;
        if ($request->created_at) $data->created_at = $request->created_at;
        if ($request->updated_by) $data->updated_by = $request->updated_by;
        if ($request->updated_at) $data->updated_at = $request->updated_at;
        $data->save();

        return $data;
    }
}
