<?php

namespace App\Http\Controllers\API\v1;

use App\Warehouse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class WarehouseController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('Name', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'Name' => 'required|max:100',
            'WarehouseType' => 'nullable|max:50',
            'Address' => 'required',
            'Phone1' => 'required|max:50',
            'Phone2' => 'nullable|max:50',
            'ContactPerson' => 'required|max:50',
            'CityCode' => 'required|max:50',
            'CountryCode' => 'nullable|max:50',
            'ZipCode' => 'nullable|max:50',
            'ActiveStatus' => 'nullable|integer|between:-9,9',
            'CreatedBy' => 'nullable|max:50',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:50',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['Code' => 'required|max:50|unique:mst_warehouse,Code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new Warehouse;
        }

        if ($request->Code) $data->Code = $request->Code;
        if ($request->Name) $data->Name = $request->Name;
        if ($request->WarehouseType) $data->WarehouseType = $request->WarehouseType;
        if ($request->Address) $data->Address = $request->Address;
        if ($request->Phone1) $data->Phone1 = $request->Phone1;
        if ($request->Phone2) $data->Phone2 = $request->Phone2;
        if ($request->ContactPerson) $data->ContactPerson = $request->ContactPerson;
        if ($request->CityCode) $data->CityCode = $request->CityCode;
        if ($request->CountryCode) $data->CountryCode = $request->CountryCode;
        if ($request->ZipCode) $data->ZipCode = $request->ZipCode;
        if ($request->ActiveStatus) $data->ActiveStatus = $request->ActiveStatus;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
