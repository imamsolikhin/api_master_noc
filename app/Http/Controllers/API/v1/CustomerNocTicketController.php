<?php

namespace App\Http\Controllers\API\v1;

use App\CustomerNocTicket;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class CustomerNocTicketController extends Controller
{
    /**
     * Septian Ramadhan | itdev.septian@gmail.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->leftJoin('mst_customer_erp','mst_customer_noc.CustomerCode','=', 'mst_customer_erp.Code')
                    ->where('mst_customer_noc.Code', 'like', '%' . $key . '%')
                    ->orWhere('mst_customer_noc.CustomerCode', 'like', '%' . $key . '%')
                    ->orWhere('mst_customer_erp.Name', 'like', '%' . $key . '%');
                    //->orWhere('MemberID', 'like', '%' . $key . '%');
    }

    /**
     * Septian Ramadhan | itdev.septian@gmail.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'CustomerCode' => 'nullable|max:250',
            'HostCode' => 'nullable|max:250',
            'TemplateCode' => 'nullable|max:250',
            'VlanDownLink' => 'nullable|max:250',
            'FrameId' => 'nullable|max:250',
            'SlotId' => 'nullable|max:250',
            'PortId' => 'nullable|max:250',
            'OntSN' => 'nullable|max:250',
            'OntId' => 'nullable|max:250',
            'OntLineProfileId' => 'nullable|max:250',
            'OntSrvProfileId' => 'nullable|max:250',
            'FdtNo' => 'nullable|max:250',
            'FatNo' => 'nullable|max:250',
            'TrafficTableId' => 'nullable|max:250',
            'NativeVlanEth1' => 'nullable|max:250',
            'NativeVlanEth2' => 'nullable|max:250',
            'NativeVlanEth3' => 'nullable|max:250',
            'NativeVlanEth4' => 'nullable|max:250',
            'LineprofileStatus' => 'nullable|max:1',
            'VlanId' => 'nullable|max:250',
            'VlanFrameId' => 'integer',
            'VlanSlotId' => 'integer',
            'VlanPortId' => 'integer',
            'VlanAttribut' => 'nullable|max:250',
            'Remark' => 'nullable',
            'ClientStatus' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|integer|between:-9,9',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['Code' => 'required|max:250|unique:mst_customer_noc,Code']);
        }

        return Validator::make($request->all(), $rules);
    }

    public function whereIn(Request $request)
    {
      $request_body=json_decode($request->getContent());
      $model = CustomerNocTicket::leftJoin('mst_customer_erp','mst_customer_noc.CustomerCode','=','mst_customer_erp.Code');
      if(!empty(@$request_body->filter_in_bts_code))
      {
        $model = $model->whereIn('mst_customer_erp.BaseTransmissionStationCode', $request_body->filter_in_bts_code);
      }
      if(!empty(@$request_body->filter_in_host_code))
      {
        if(empty(@$request_body->filter_in_bts_code))
        {
          $model = $model->WhereIn('mst_customer_noc.HostCode', $request_body->filter_in_host_code);
        }
        else {
          $model = $model->orWhereIn('mst_customer_noc.HostCode', $request_body->filter_in_host_code);
        }

      }
      $result = $model->get();
      return response()->json(array('data'=>$result), 200);
      // if($request->filter_in_bts_code, $request->filter_in_host_code)
      // {
      //   //CustomerNocTicket
      // }
      // return makeResponse(200, 'pagination', null, getResourceName($table)::collection($data));
    }

    /**
     * Septian Ramadhan | itdev.septian@gmail.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
}
