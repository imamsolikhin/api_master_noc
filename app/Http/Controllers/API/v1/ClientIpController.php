<?php

namespace App\Http\Controllers\API\v1;

use App\ClientIp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ClientIpController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('name', 'like', '%' . $key . '%')
                    ->orWhere('ip', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'code' => 'nullable|max:255',
            'clients_id' => 'nullable|integer',
            'orderno' => 'nullable|integer',
            'ip_type' => 'nullable|max:255',
            'name' => 'nullable|max:255',
            'ip' => 'nullable|max:255',
            'note' => 'nullable|max:255',
            'disabled' => 'nullable|max:1',
            'created_on' => 'nullable|date_format:Y-m-d H:i:s',
            'port' => 'nullable|max:255',
            'ActiveStatus' => 'nullable|integer|between:-9,9',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new ClientIp;
        }

        if ($request->code) $data->code = $request->code;
        if ($request->clients_id) $data->clients_id = $request->clients_id;
        if ($request->orderno) $data->orderno = $request->orderno;
        if ($request->ip_type) $data->ip_type = $request->ip_type;
        if ($request->name) $data->name = $request->name;
        if ($request->ip) $data->ip = $request->ip;
        if ($request->note) $data->note = $request->note;
        if ($request->disabled) $data->disabled = $request->disabled;
        if ($request->created_on) $data->created_on = $request->created_on;
        if ($request->port) $data->port = $request->port;
        if ($request->ActiveStatus) $data->ActiveStatus = $request->ActiveStatus;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
