<?php

namespace App\Http\Controllers\API\v1;

use App\TrafficTable;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TrafficTableController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('name', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'code' => 'nullable|max:255',
            'host' => 'nullable|max:255',
            'index' => 'nullable|integer',
            'name' => 'nullable|max:255',
            'cir' => 'nullable|max:255',
            'cbs' => 'nullable|max:255',
            'pir' => 'nullable|max:255',
            'pbs' => 'nullable|max:255',
            'color_mode' => 'nullable|max:255',
            'priority' => 'nullable|max:255',
            'priority_policy' => 'nullable|max:255',
            'disabled' => 'nullable|max:1',
            'created_on' => 'nullable|date_format:Y-m-d H:i:s',
            'hostname' => 'nullable|max:255',
            'ActiveStatus' => 'nullable|integer|between:-9,9',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new TrafficTable;
        }

        if ($request->code) $data->code = $request->code;
        if ($request->host) $data->host = $request->host;
        if ($request->index) $data->index = $request->index;
        if ($request->name) $data->name = $request->name;
        if ($request->cir) $data->cir = $request->cir;
        if ($request->cbs) $data->cbs = $request->cbs;
        if ($request->pir) $data->pir = $request->pir;
        if ($request->pbs) $data->pbs = $request->pbs;
        if ($request->color_mode) $data->color_mode = $request->color_mode;
        if ($request->priority) $data->priority = $request->priority;
        if ($request->priority_policy) $data->priority_policy = $request->priority_policy;
        if ($request->disabled) $data->disabled = $request->disabled;
        if ($request->created_on) $data->created_on = $request->created_on;
        if ($request->hostname) $data->hostname = $request->hostname;
        if ($request->ActiveStatus) $data->ActiveStatus = $request->ActiveStatus;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
