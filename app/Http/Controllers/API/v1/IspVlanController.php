<?php

namespace App\Http\Controllers\API\v1;

use App\IspVlan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class IspVlanController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('isp', 'like', '%' . $key . '%')
                    ->orWhere('vlan', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'code' => 'nullable|max:255',
            'isp' => 'nullable|integer',
            'vlan' => 'nullable|integer',
            'note' => 'nullable',
            'disabled' => 'nullable|max:1',
            'created_on' => 'nullable|date_format:Y-m-d H:i:s',
            'host' => 'nullable|integer',
            'hostname' => 'nullable|max:255',
            'vlan_frame_id' => 'nullable|max:255',
            'vlan_slot_id' => 'nullable|max:255',
            'vlan_port_id' => 'nullable|max:255',
            'ActiveStatus' => 'nullable|integer|between:-9,9',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new IspVlan;
        }

        if ($request->code) $data->code = $request->code;
        if ($request->isp) $data->isp = $request->isp;
        if ($request->vlan) $data->vlan = $request->vlan;
        if ($request->note) $data->note = $request->note;
        if ($request->disabled) $data->disabled = $request->disabled;
        if ($request->created_on) $data->created_on = $request->created_on;
        if ($request->host) $data->host = $request->host;
        if ($request->hostname) $data->hostname = $request->hostname;
        if ($request->vlan_frame_id) $data->vlan_frame_id = $request->vlan_frame_id;
        if ($request->vlan_slot_id) $data->vlan_slot_id = $request->vlan_slot_id;
        if ($request->vlan_port_id) $data->vlan_port_id = $request->vlan_port_id;
        if ($request->ActiveStatus) $data->ActiveStatus = $request->ActiveStatus;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
