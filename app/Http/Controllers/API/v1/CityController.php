<?php

namespace App\Http\Controllers\API\v1;

use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CityController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('Name', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'Name' => 'nullable|max:250',
            'ProvinceCode' => 'nullable|max:250',
            'ActiveStatus' => 'nullable|integer|between:-9,9',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'InActiveBy' => 'nullable|max:250',
            'InActiveDate' => 'nullable|date_format:Y-m-d H:i:s',
            'Remark' => 'nullable',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['Code' => 'required|max:250|unique:mst_city,Code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new City;
        }

        if ($request->Code) $data->Code = $request->Code;
        if ($request->Name) $data->Name = $request->Name;
        if ($request->ProvinceCode) $data->ProvinceCode = $request->ProvinceCode;
        if ($request->ActiveStatus) $data->ActiveStatus = $request->ActiveStatus;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        if ($request->InActiveBy) $data->InActiveBy = $request->InActiveBy;
        if ($request->InActiveDate) $data->InActiveDate = $request->InActiveDate;
        if ($request->Remark) $data->Remark = $request->Remark;
        $data->save();

        return $data;
    }
}
