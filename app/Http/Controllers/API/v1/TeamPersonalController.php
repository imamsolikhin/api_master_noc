<?php

namespace App\Http\Controllers\API\v1;

use App\TeamPersonal;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TeamPersonalController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('name', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'name' => 'nullable|max:250',
            'teamcode' => 'nullable|max:250',
            'jobpositioncode' => 'nullable|max:250',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['code' => 'required|max:250|unique:mst_team_personal,code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new TeamPersonal;
        }

        if ($request->code) $data->code = $request->code;
        if ($request->name) $data->name = $request->name;
        if ($request->teamcode) $data->teamcode = $request->teamcode;
        if ($request->jobpositioncode) $data->jobpositioncode = $request->jobpositioncode;
        $data->save();

        return $data;
    }
}
