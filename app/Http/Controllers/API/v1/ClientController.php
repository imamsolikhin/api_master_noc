<?php

namespace App\Http\Controllers\API\v1;

use App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('isp', 'like', '%' . $key . '%')
                    ->orWhere('name', 'like', '%' . $key . '%')
                    ->orWhere('status', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'isp' => 'nullable|max:255',
            'idp' => 'nullable|max:255',
            'name' => 'nullable|max:255',
            'alamat' => 'nullable',
            'status' => 'nullable|max:255',
            'traffic_table' => 'nullable|max:255',
            'note' => 'nullable',
            'host' => 'nullable|max:255',
            'frame_id' => 'nullable|max:255',
            'slot_id' => 'nullable|max:255',
            'port_id' => 'nullable|max:255',
            'ont_id' => 'nullable|integer',
            'sn_auth' => 'nullable|max:255',
            'management_mode' => 'nullable|max:255',
            'ont_lineprofile_id' => 'nullable|max:255',
            'ont_srvprofile_id' => 'nullable|max:255',
            'description' => 'nullable|max:255',
            'disabled' => 'nullable|max:1',
            'created_on' => 'nullable|date_format:Y-m-d H:i:s',
            'board_id' => 'nullable|integer',
            'ont_lineprofile_name' => 'nullable|max:255',
            'ont_srvprofile_name' => 'nullable|max:255',
            'hostname' => 'nullable|max:255',
            'ont_autofind' => 'nullable|max:255',
            'traffic_table_cir' => 'nullable|max:255',
            'vendor_id' => 'nullable|max:255',
            'ont_version' => 'nullable|max:255',
            'ont_software_version' => 'nullable|max:255',
            'ont_equipment_id' => 'nullable|max:255',
            'native_vlan_eth_1' => 'nullable|max:255',
            'native_vlan_eth_2' => 'nullable|max:255',
            'native_vlan_eth_3' => 'nullable|max:255',
            'native_vlan_eth_4' => 'nullable|max:255',
            'vlan_1' => 'nullable|max:255',
            'gemport_1' => 'nullable|max:255',
            'user_vlan_1' => 'nullable|max:255',
            'tag_transform_1' => 'nullable|max:255',
            'vlan_2' => 'nullable|max:255',
            'gemport_2' => 'nullable|max:255',
            'user_vlan_2' => 'nullable|max:255',
            'tag_transform_2' => 'nullable|max:255',
            'traffic_table_1' => 'nullable|max:255',
            'traffic_table_2' => 'nullable|max:255',
            'inner_vlan_1' => 'nullable|max:255',
            'inner_vlan_2' => 'nullable|max:255',
            'index_1' => 'nullable|max:255',
            'index_2' => 'nullable|max:255',
            'ont_count' => 'nullable|integer',
            'template' => 'nullable|max:255',
            'template_vlan' => 'nullable|max:255',
            'add_vlan' => 'nullable|max:1',
            'add_vlan_service' => 'nullable|max:1',
            'add_vlan_line' => 'nullable|max:1',
            'vlan_frame_id' => 'nullable|max:255',
            'vlan_slot_id' => 'nullable|max:255',
            'vlan_port_id' => 'nullable|max:255',
            'add_traffic_table' => 'nullable|max:1',
        ];

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new Client;
        }

        if ($request->isp) $data->isp = $request->isp;
        if ($request->idp) $data->idp = $request->idp;
        if ($request->name) $data->name = $request->name;
        if ($request->alamat) $data->alamat = $request->alamat;
        if ($request->status) $data->status = $request->status;
        if ($request->traffic_table) $data->traffic_table = $request->traffic_table;
        if ($request->note) $data->note = $request->note;
        if ($request->host) $data->host = $request->host;
        if ($request->frame_id) $data->frame_id = $request->frame_id;
        if ($request->slot_id) $data->slot_id = $request->slot_id;
        if ($request->port_id) $data->port_id = $request->port_id;
        if ($request->ont_id) $data->ont_id = $request->ont_id;
        if ($request->sn_auth) $data->sn_auth = $request->sn_auth;
        if ($request->management_mode) $data->management_mode = $request->management_mode;
        if ($request->ont_lineprofile_id) $data->ont_lineprofile_id = $request->ont_lineprofile_id;
        if ($request->ont_srvprofile_id) $data->ont_srvprofile_id = $request->ont_srvprofile_id;
        if ($request->description) $data->description = $request->description;
        if ($request->disabled) $data->disabled = $request->disabled;
        if ($request->created_on) $data->created_on = $request->created_on;
        if ($request->board_id) $data->board_id = $request->board_id;
        if ($request->ont_lineprofile_name) $data->ont_lineprofile_name = $request->ont_lineprofile_name;
        if ($request->ont_srvprofile_name) $data->ont_srvprofile_name = $request->ont_srvprofile_name;
        if ($request->hostname) $data->hostname = $request->hostname;
        if ($request->ont_autofind) $data->ont_autofind = $request->ont_autofind;
        if ($request->traffic_table_cir) $data->traffic_table_cir = $request->traffic_table_cir;
        if ($request->vendor_id) $data->vendor_id = $request->vendor_id;
        if ($request->ont_version) $data->ont_version = $request->ont_version;
        if ($request->ont_software_version) $data->ont_software_version = $request->ont_software_version;
        if ($request->ont_equipment_id) $data->ont_equipment_id = $request->ont_equipment_id;
        if ($request->native_vlan_eth_1) $data->native_vlan_eth_1 = $request->native_vlan_eth_1;
        if ($request->native_vlan_eth_2) $data->native_vlan_eth_2 = $request->native_vlan_eth_2;
        if ($request->native_vlan_eth_3) $data->native_vlan_eth_3 = $request->native_vlan_eth_3;
        if ($request->native_vlan_eth_4) $data->native_vlan_eth_4 = $request->native_vlan_eth_4;
        if ($request->vlan_1) $data->vlan_1 = $request->vlan_1;
        if ($request->gemport_1) $data->gemport_1 = $request->gemport_1;
        if ($request->user_vlan_1) $data->user_vlan_1 = $request->user_vlan_1;
        if ($request->tag_transform_1) $data->tag_transform_1 = $request->tag_transform_1;
        if ($request->vlan_2) $data->vlan_2 = $request->vlan_2;
        if ($request->gemport_2) $data->gemport_2 = $request->gemport_2;
        if ($request->user_vlan_2) $data->user_vlan_2 = $request->user_vlan_2;
        if ($request->tag_transform_2) $data->tag_transform_2 = $request->tag_transform_2;
        if ($request->traffic_table_1) $data->traffic_table_1 = $request->traffic_table_1;
        if ($request->traffic_table_2) $data->traffic_table_2 = $request->traffic_table_2;
        if ($request->inner_vlan_1) $data->inner_vlan_1 = $request->inner_vlan_1;
        if ($request->inner_vlan_2) $data->inner_vlan_2 = $request->inner_vlan_2;
        if ($request->index_1) $data->index_1 = $request->index_1;
        if ($request->index_2) $data->index_2 = $request->index_2;
        if ($request->ont_count) $data->ont_count = $request->ont_count;
        if ($request->template) $data->template = $request->template;
        if ($request->template_vlan) $data->template_vlan = $request->template_vlan;
        if ($request->add_vlan) $data->add_vlan = $request->add_vlan;
        if ($request->add_vlan_service) $data->add_vlan_service = $request->add_vlan_service;
        if ($request->add_vlan_line) $data->add_vlan_line = $request->add_vlan_line;
        if ($request->vlan_frame_id) $data->vlan_frame_id = $request->vlan_frame_id;
        if ($request->vlan_slot_id) $data->vlan_slot_id = $request->vlan_slot_id;
        if ($request->vlan_port_id) $data->vlan_port_id = $request->vlan_port_id;
        if ($request->add_traffic_table) $data->add_traffic_table = $request->add_traffic_table;
        $data->save();

        return $data;
    }
}
