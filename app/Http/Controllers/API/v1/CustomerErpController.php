<?php

namespace App\Http\Controllers\API\v1;

use App\CustomerErp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CustomerErpController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('Name', 'like', '%' . $key . '%')
                    ->orWhere('Address', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'Name' => 'nullable|max:250',
            'ContactPerson' => 'nullable|max:250',
            'Address' => 'nullable',
            'CityCode' => 'nullable|max:250',
            'Phone1' => 'nullable|max:250',
            'Phone2' => 'nullable|max:250',
            'Fax' => 'nullable|max:250',
            'Email' => 'nullable|max:250',
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['Code' => 'required|max:250|unique:mst_customer_erp,Code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new CustomerErp;
        }

        if ($request->Code) $data->Code = $request->Code;
        if ($request->Name) $data->Name = $request->Name;
        if ($request->ContactPerson) $data->ContactPerson = $request->ContactPerson;
        if ($request->Address) $data->Address = $request->Address;
        if ($request->CityCode) $data->CityCode = $request->CityCode;
        if ($request->Phone1) $data->Phone1 = $request->Phone1;
        if ($request->Phone2) $data->Phone2 = $request->Phone2;
        if ($request->Fax) $data->Fax = $request->Fax;
        if ($request->Email) $data->Email = $request->Email;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
