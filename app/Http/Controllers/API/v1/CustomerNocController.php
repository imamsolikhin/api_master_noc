<?php

namespace App\Http\Controllers\API\v1;

use App\CustomerNoc;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CustomerNocController extends Controller
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Queries for Search.
     *
     * @param  Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $key
     * @return Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery($query, $key)
    {
        return $query->where('IPLocalAP', 'like', '%' . $key . '%')
                    ->orWhere('IPLocalCPE', 'like', '%' . $key . '%')
                    ->orWhere('IPPublic', 'like', '%' . $key . '%');
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Set Validation From Request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $type
     * @return Illuminate\Validation\Validator
     */
    public static function validation($request, $type = null)
    {
        $rules = [
            'IPLocalAP' => 'nullable|string|max:250',
            'IPLocalCPE' => 'nullable|string|max:250',
            'IPPublic' => 'nullable|string|max:250',
            'NIBTSCode' => 'nullable|string|max:250',
            'NIVLANID' => 'nullable|string|max:250',
            'AvlAP' => 'nullable|numeric',
            'AvlCPE' => 'nullable|numeric',
            'AvlRouter' => 'nullable|numeric',
            'Border1Code' => 'nullable|string|max:250',
            'Border2Code' => 'nullable|string|max:250',
            'Border3Code' => 'nullable|string|max:250',
            'DistributionCode' => 'nullable|string|max:250',
            'BTSCode' => 'nullable|string|max:250',
            'Shaper1Code' => 'nullable|string|max:250',
            'Shaper2Code' => 'nullable|string|max:250',
            'Shaper3Code' => 'nullable|string|max:250',
            'VLANID' => 'nullable|string|max:250',
            'VMANID' => 'nullable|string|max:250'
            'CreatedBy' => 'nullable|max:250',
            'CreatedDate' => 'nullable|date_format:Y-m-d H:i:s',
            'UpdatedBy' => 'nullable|max:250',
            'UpdatedDate' => 'nullable|date_format:Y-m-d H:i:s',
        ];

        if (is_null($type)) {
            $rules = array_merge($rules, ['Code' => 'required|max:250|unique:mst_customer_noc,Code']);
        }

        return Validator::make($request->all(), $rules);
    }

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Save resource in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  object|null  $data
     * @return object
     */
    public static function save($request, $data = null)
    {
        if (is_null($data)) {
            $data = new CustomerNoc;
        }

        if ($request->IPLocalAP) $data->IPLocalAP = $request->IPLocalAP;
        if ($request->IPLocalCPE) $data->IPLocalCPE = $request->IPLocalCPE;
        if ($request->IPPublic) $data->IPPublic = $request->IPPublic;
        if ($request->NIDistribution) $data->NIDistribution = $request->NIDistribution;
        if ($request->NIBTSCode) $data->NIBTSCode = $request->NIBTSCode;
        if ($request->NIVLANID) $data->NIVLANID = $request->NIVLANID;
        if ($request->AvlAP) $data->AvlAP = $request->AvlAP;
        if ($request->AvlCPE) $data->AvlCPE = $request->AvlCPE;
        if ($request->AvlRouter) $data->AvlRouter = $request->AvlRouter;
        if ($request->Border1Code) $data->Border1Code = $request->Border1Code;
        if ($request->Border2Code) $data->Border2Code = $request->Border2Code;
        if ($request->Border3Code) $data->Border3Code = $request->Border3Code;
        if ($request->DistributionCode) $data->DistributionCode = $request->DistributionCode;
        if ($request->ConsentratorCode) $data->ConsentratorCode = $request->ConsentratorCode;
        if ($request->BTSCode) $data->BTSCode = $request->BTSCode;
        if ($request->VLANID) $data->VLANID = $request->VLANID;
        if ($request->VMANID) $data->VMANID = $request->VMANID;
        if ($request->Shaper1Code) $data->Shaper1Code = $request->Shaper1Code;
        if ($request->Shaper2Code) $data->Shaper2Code = $request->Shaper2Code;
        if ($request->Shaper3Code) $data->Shaper3Code = $request->Shaper3Code;
        if ($request->CreatedBy) $data->CreatedBy = $request->CreatedBy;
        if ($request->CreatedDate) $data->CreatedDate = $request->CreatedDate;
        if ($request->UpdatedBy) $data->UpdatedBy = $request->UpdatedBy;
        if ($request->UpdatedDate) $data->UpdatedDate = $request->UpdatedDate;
        $data->save();

        return $data;
    }
}
