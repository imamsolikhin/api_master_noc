<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Warehouse extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Code' => $this->Code,
            'Name' => $this->Name,
            'WarehouseType' => $this->WarehouseType,
            'Address' => $this->Address,
            'Phone1' => $this->Phone1,
            'Phone2' => $this->Phone2,
            'ContactPerson' => $this->ContactPerson,
            'CityCode' => $this->CityCode,
            'CountryCode' => $this->CountryCode,
            'ZipCode' => $this->ZipCode,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }
}