<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Branch extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Code' => $this->Code,
            'Name' => $this->Name,
            'Address' => $this->Address,
            'CityCode' => $this->CityCode,
            'ZipCode' => $this->ZipCode,
            'Phone1' => $this->Phone1,
            'Phone2' => $this->Phone2,
            'Fax' => $this->Fax,
            'EmailAddress' => $this->EmailAddress,
            'ContactPerson' => $this->ContactPerson,
            'DefaultBillToCode' => $this->DefaultBillToCode,
            'DefaultShipToCode' => $this->DefaultShipToCode,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
            'Remark' => $this->Remark,
        ];
    }
}