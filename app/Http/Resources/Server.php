<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Server extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'name' => $this->name,
            'rpc_endpoint' => $this->rpc_endpoint,
            'note' => $this->note,
            'created_on' => date('Y-m-d H:i:s', strtotime($this->created_on)),
            'wamp_registration_id' => $this->wamp_registration_id,
            'ip_address' => $this->ip_address,
            'location' => $this->location,
            'disabled' => $this->disabled,
            'dev_uplink' => $this->dev_uplink,
            'dev_downlink' => $this->dev_downlink,
            'class_group' => $this->class_group,
            'class_attr' => $this->class_attr,
            'service' => $this->service,
            'ssh_user' => $this->ssh_user,
            'ssh_pass' => $this->ssh_pass,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }
}