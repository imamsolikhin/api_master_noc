<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Item extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Code' => $this->Code,
            'Name' => $this->Name,
            'PackageStatus' => $this->PackageStatus,
            'ItemBrandCode' => $this->ItemBrandCode,
            'ItemSubCategoryCode' => $this->ItemSubCategoryCode,
            'UnitOfMeasureCode' => $this->UnitOfMeasureCode,
            'InventoryType' => $this->InventoryType,
            'SerialNoStatus' => $this->SerialNoStatus,
            'MinStock' => $this->MinStock,
            'MaxStock' => $this->MaxStock,
            'PackingCode' => $this->PackingCode,
            'PackingQuantity' => $this->PackingQuantity,
            'KgQuantity' => $this->KgQuantity,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
            'InActiveBy' => $this->InActiveBy,
            'InActiveDate' => $this->InActiveDate != null ? date('Y-m-d H:i:s', strtotime($this->InActiveDate)) : null,
            'Remark' => $this->Remark,
        ];
    }
}