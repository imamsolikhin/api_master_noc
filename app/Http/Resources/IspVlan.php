<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class IspVlan extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'isp' => $this->isp,
            'vlan' => $this->vlan,
            'note' => $this->note,
            'disabled' => $this->disabled,
            'created_on' => date('Y-m-d H:i:s', strtotime($this->created_on)),
            'host' => $this->host,
            'hostname' => $this->hostname,
            'vlan_frame_id' => $this->vlan_frame_id,
            'vlan_slot_id' => $this->vlan_slot_id,
            'vlan_port_id' => $this->vlan_port_id,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }
}