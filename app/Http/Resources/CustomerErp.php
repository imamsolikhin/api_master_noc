<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerErp extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Code' => $this->Code,
            'Name' => $this->Name,
            'ContactPerson' => $this->ContactPerson,
            'Address' => $this->Address,
            'CityCode' => $this->CityCode,
            'Phone1' => $this->Phone1,
            'Phone2' => $this->Phone2,
            'Fax' => $this->Fax,
            'Email' => $this->Email,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate,
        ];
    }
}