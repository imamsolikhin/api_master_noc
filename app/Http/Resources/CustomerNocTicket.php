<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerNocTicket extends JsonResource
{
    /**
     * Septian Ramadhan | itdev.septian@gmail.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'Code'=>$this->Code,
          'CustomerCode'=>$this->CustomerCode,
          'HostCode'=>$this->HostCode,
          'TemplateCode'=>$this->TemplateCode,
          'VlanDownLink'=>$this->VlanDownLink,
          'FrameId'=>$this->FrameId,
          'SlotId'=>$this->SlotId,
          'PortId'=>$this->PortId,
          'OntSN'=>$this->OntSN,
          'OntId'=>$this->OntId,
          'OntLineProfileId'=>$this->OntLineProfileId,
          'OntSrvProfileId'=>$this->OntSrvProfileId,
          'FdtNo'=>$this->FdtNo,
          'FatNo'=>$this->FatNo,
          'TrafficTableId'=>$this->TrafficTableId,
          'NativeVlanEth1'=>$this->NativeVlanEth1,
          'NativeVlanEth2'=>$this->NativeVlanEth2,
          'NativeVlanEth3'=>$this->NativeVlanEth3,
          'NativeVlanEth4'=>$this->NativeVlanEth4,
          'LineprofileStatus'=>$this->LineprofileStatus,
          'VlanId'=>$this->VlanId,
          'VlanFrameId'=>$this->VlanFrameId,
          'VlanSlotId'=>$this->VlanSlotId,
          'VlanPortId'=>$this->VlanPortId,
          'VlanAttribut'=>$this->VlanAttribut,
          'Remark'=>$this->Remark,
          'ClientStatus'=>$this->ClientStatus,
          'ActiveStatus'=>$this->ActiveStatus,
          'CreatedBy'=>$this->CreatedBy,
          'CreatedDate'=>$this->CreatedDate,
          'UpdatedBy'=>$this->UpdatedBy,
          'UpdatedDate'=>$this->UpdatedDate,
          'Name'=>$this->Name
        ];
    }
}
