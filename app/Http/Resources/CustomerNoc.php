<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerNoc extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Code' => $this->Code,
            'IPLocalAP' => $this->IPLocalAP,
            'IPLocalCPE' => $this->IPLocalCPE,
            'IPPublic' => $this->IPPublic,
            'NIDistribution' => $this->NIDistribution,
            'NIBTSCode' => $this->NIBTSCode,
            'NIVLANID' => $this->NIVLANID,
            'AvlAP' => $this->AvlAP,
            'AvlCPE' => $this->AvlCPE,
            'AvlRouter' => $this->AvlRouter,
            'Border1Code' => $this->Border1Code,
            'Border2Code' => $this->Border2Code,
            'Border3Code' => $this->Border3Code,
            'DistributionCode' => $this->DistributionCode,
            'ConsentratorCode' => $this->ConsentratorCode,
            'BTSCode' => $this->BTSCode,
            'VLANID' => $this->VLANID,
            'VMANID' => $this->VMANID,
            'Shaper1Code' => $this->Shaper1Code,
            'Shaper2Code' => $this->Shaper2Code,
            'Shaper3Code' => $this->Shaper3Code,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate,
        ];
    }
}