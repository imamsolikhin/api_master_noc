<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Client extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'isp' => $this->isp,
            'idp' => $this->idp,
            'name' => $this->name,
            'alamat' => $this->alamat,
            'status' => $this->status,
            'traffic_table' => $this->traffic_table,
            'note' => $this->note,
            'host' => $this->host,
            'frame_id' => $this->frame_id,
            'slot_id' => $this->slot_id,
            'port_id' => $this->port_id,
            'ont_id' => $this->ont_id,
            'sn_auth' => $this->sn_auth,
            'management_mode' => $this->management_mode,
            'ont_lineprofile_id' => $this->ont_lineprofile_id,
            'ont_srvprofile_id' => $this->ont_srvprofile_id,
            'description' => $this->description,
            'disabled' => $this->disabled,
            'created_on' => date('Y-m-d H:i:s', strtotime($this->created_on)),
            'board_id' => $this->board_id,
            'ont_lineprofile_name' => $this->ont_lineprofile_name,
            'ont_srvprofile_name' => $this->ont_srvprofile_name,
            'hostname' => $this->hostname,
            'ont_autofind' => $this->ont_autofind,
            'traffic_table_cir' => $this->traffic_table_cir,
            'vendor_id' => $this->vendor_id,
            'ont_version' => $this->ont_version,
            'ont_software_version' => $this->ont_software_version,
            'ont_equipment_id' => $this->ont_equipment_id,
            'native_vlan_eth_1' => $this->native_vlan_eth_1,
            'native_vlan_eth_2' => $this->native_vlan_eth_2,
            'native_vlan_eth_3' => $this->native_vlan_eth_3,
            'native_vlan_eth_4' => $this->native_vlan_eth_4,
            'vlan_1' => $this->vlan_1,
            'gemport_1' => $this->gemport_1,
            'user_vlan_1' => $this->user_vlan_1,
            'tag_transform_1' => $this->tag_transform_1,
            'vlan_2' => $this->vlan_2,
            'gemport_2' => $this->gemport_2,
            'user_vlan_2' => $this->user_vlan_2,
            'tag_transform_2' => $this->tag_transform_2,
            'traffic_table_1' => $this->traffic_table_1,
            'traffic_table_2' => $this->traffic_table_2,
            'inner_vlan_1' => $this->inner_vlan_1,
            'inner_vlan_2' => $this->inner_vlan_2,
            'index_1' => $this->index_1,
            'index_2' => $this->index_2,
            'ont_count' => $this->ont_count,
            'template' => $this->template,
            'template_vlan' => $this->template_vlan,
            'add_vlan' => $this->add_vlan,
            'add_vlan_service' => $this->add_vlan_service,
            'add_vlan_line' => $this->add_vlan_line,
            'vlan_frame_id' => $this->vlan_frame_id,
            'vlan_slot_id' => $this->vlan_slot_id,
            'vlan_port_id' => $this->vlan_port_id,
            'add_traffic_table' => $this->add_traffic_table,
        ];
    }
}