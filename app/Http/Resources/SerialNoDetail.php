<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SerialNoDetail extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Code' => $this->Code,
            'BranchCode' => $this->BranchCode,
            'WarehouseCode' => $this->WarehouseCode,
            'SerialNo' => $this->SerialNo,
            'ItemCode' => $this->ItemCode,
            'ActualStock' => $this->ActualStock,
            'COGSIDR' => $this->COGSIDR,
            'LocationCode' => $this->LocationCode,
            'RemarkText' => $this->RemarkText,
            'RemarkDate' => $this->RemarkDate != null ? date('Y-m-d H:i:s', strtotime($this->RemarkDate)) : null,
            'RemarkQuantity' => $this->RemarkQuantity,
            'InDocumentType' => $this->InDocumentType,
            'InTransactionNo' => $this->InTransactionNo,
            'InTransactionDate' => $this->InTransactionDate != null ? date('Y-m-d H:i:s', strtotime($this->InTransactionDate)) : null,
            'OutDocumentType' => $this->OutDocumentType,
            'OutTransactionNo' => $this->OutTransactionNo,
            'OutTransactionDate' => $this->OutTransactionDate != null ? date('Y-m-d H:i:s', strtotime($this->OutTransactionDate)) : null,
            'OutStatus' => $this->OutStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }
}