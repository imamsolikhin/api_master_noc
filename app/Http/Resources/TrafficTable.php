<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrafficTable extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'host' => $this->host,
            'index' => $this->index,
            'name' => $this->name,
            'cir' => $this->cir,
            'cbs' => $this->cbs,
            'pir' => $this->pir,
            'pbs' => $this->pbs,
            'color_mode' => $this->color_mode,
            'priority' => $this->priority,
            'priority_policy' => $this->priority_policy,
            'disabled' => $this->disabled,
            'created_on' => date('Y-m-d H:i:s', strtotime($this->created_on)),
            'hostname' => $this->hostname,
            'ActiveStatus' => $this->ActiveStatus,
            'CreatedBy' => $this->CreatedBy,
            'CreatedDate' => $this->CreatedDate != null ? date('Y-m-d H:i:s', strtotime($this->CreatedDate)) : null,
            'UpdatedBy' => $this->UpdatedBy,
            'UpdatedDate' => $this->UpdatedDate != null ? date('Y-m-d H:i:s', strtotime($this->UpdatedDate)) : null,
        ];
    }
}