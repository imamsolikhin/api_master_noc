<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Isp extends JsonResource
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Transform the resource into an array.
     *
     * @param  Illuminate\Database\Eloquent\Model  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'address' => $this->address,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at != null ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at != null ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}