<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use phpseclib\Crypt\RSA;

class SignatureMiddleware
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        if ($request->header('Signature')) {
            $rsa = new RSA();
            $rsa->setHash('sha256');
            $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1);
            $rsa->loadKey(config('secret.public_key'));

            try {
                $signature = base64_decode($request->header('Signature'));

                if ($request->isMethod('GET') || $request->isMethod('DELETE')) {
                    $plaintext = $request->fullUrl();
                } else {
                    $plaintext = json_encode($request->all());
                }

                if (!$rsa->verify($plaintext, $signature)) return makeResponse(422, 'error', 'signature is not match');

                return $next($request);
            } catch (Exception $e) {
                return makeResponse(422, 'error', 'signature cannot be processed');
            }
        }

        return makeResponse(401, 'error', 'unverified');
    }
}
