<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerNoc extends Model
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_customer_noc';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'Code';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Indicates if the primary key is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "created at" column.
     *
     * @const string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "updated at" column.
     *
     * @const string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'IPLocalAP', 'IPLocalCPE', 'IPPublic', 'NIDistribution', 'NIBTSCode', 'NIVLANID', 'AvlAP', 'AvlCPE', 'AvlRouter', 'Border1Code', 'Border2Code', 'Border3Code', 'DistributionCode', 'ConsentratorCode', 'BTSCode', 'VLANID', 'VMANID', 'Shaper1Code', 'Shaper2Code', 'Shaper3Code', 'CreatedBy', 'CreatedDate', 'UpdatedBy', 'UpdatedDate'
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
