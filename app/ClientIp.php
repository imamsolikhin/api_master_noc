<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientIp extends Model
{
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mst_clients_ip';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "created at" column.
     *
     * @const string
     */
    const CREATED_AT = 'CreatedDate';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The name of the "updated at" column.
     *
     * @const string
     */
    const UPDATED_AT = 'UpdatedDate';

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'clients_id', 'orderno', 'ip_type', 'name', 'ip', 'note', 'disabled', 'created_on', 'port', 'ActiveStatus', 'CreatedBy', 'CreatedDate', 'UpdatedBy', 'UpdatedDate',
    ];

    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
