<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Route for Documentation.
 */

$router->get('/', function () {
    return redirect('https://documenter.getpostman.com/view/5385605/T17Na4jG');
});

/**
 * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
 * Route Group for API.
 */
$router->group(['prefix' => 'api', 'namespace' => 'API', 'middleware' => 'api'], function () use ($router) {
    /**
     * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
     * Route Group for Version 1.
     */
    $router->group(['prefix' => 'v1', 'namespace' => 'v1', 'middleware' => 'signature'], function () use ($router) {
        /**
         * Andrea Adam | andreaadam215@gmail.com | mrandreid.com
         * Route for Master.
         */
        $router->group(['prefix' => '{table}'], function () use ($router) {
            $router->get('/', [
                'as' => 'master.index', 'uses' => 'MasterController@index'
            ]);
            $router->post('/', [
                'as' => 'master.store', 'uses' => 'MasterController@store'
            ]);
            $router->get('{id}', [
                'as' => 'master.show', 'uses' => 'MasterController@show'
            ]);
            $router->put('{id}', [
                'as' => 'master.update', 'uses' => 'MasterController@update'
            ]);
            $router->delete('{id}', [
                'as' => 'master.destroy', 'uses' => 'MasterController@destroy'
            ]);
        });

        $router->post('CustomerNocTicket/getIn', [
            'as' => 'cnt.whereIn', 'uses' => 'CustomerNocTicketController@whereIn'
        ]);
    });
});
